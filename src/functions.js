import Api from './dApi'
const devConfig = require('./../config/devConfig.js')

let $Api = new Api(devConfig, devConfig.baseUser)

/**
 * Create an article
 * @param title
 * @param body
 */
let createArticle = (title, body) => {
  $Api.create.article.setFields({
    title: [{
      value: title
    }],
    body: [{
      value: body,
      format: 'basic_html',
      summary: ''
    }]
  }).post()
}

/**
 * Delete an article
 * @param nid
 * @returns {Promise}
 */
let deleteArticle = (nid) => {
  return $Api.delete.article.delete(nid)
}

export { createArticle, deleteArticle }