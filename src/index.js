import Api from './dApi'
import {article as articleFields, page as pageFields} from './testContent'
import './functions'
import {createArticle, deleteArticle} from "./functions";

// Demo user
let user = { name: 'admin', pass: 'admin' }
// Instance Api class
let $Api = new Api({ url: 'http://127.0.0.1:8088' }, user)

// Check if user is already logged; if not then log it
if (!$Api.drupalUser.isLogged()) {
  $Api.drupalUser.login().then((data) => {})
}
// Extends api to add custom entity
$Api.addEntity('node', 'custom_node_type', 'default')
// In this way it's possible to do: $Api.create.custom_node_type or $Api.delete.custom_node_type and so on...


createArticle('Article created with dApi.js', 'This is an article created using dApi.js library');
// deleteArticle(40);

if (module.hot) {
  module.hot.accept();
}