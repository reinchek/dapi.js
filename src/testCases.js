import Api from './dApi'
const devConfig = require('./../config/devConfig.js')

let $Api = new Api(devConfig, devConfig.baseUser)

/**
 * Test cases
 */
export default {
  /**
   * Create a node of specic type
   * @param {string} type - Node's type (es: 'article')
   * @param {string} title - Node's title
   */
  create: function (type, title) {
    return $Api.create[type].setFields({
      title: [{
        value: title
      }],
    }).post()

  },
  /**
   * Update a node of specic type
   * @param {string} type - Node's type (es: 'article')
   * @param {number} nid - Node's nid to update
   * @param {string} title - Node's new title
   */
  update: function (type, nid, title) {
    $Api.update[type].prepare().then(function (action) {
      action.setFields({
        title: [{
          value: title
        }]
      }).post(nid)
    })
  },
  /**
   * Delete a node of specic type
   * @param {string} type - Node's type (es: 'article')
   * @param {number} nid - Node's nid to delete
   */
  delete: function (type, nid) {
    $Api.delete[article].prepare().then(function (action) {
      action.delete(nid)
    })
  }
};