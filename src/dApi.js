/**
 * @file dApi.js it's a JavaScript library to interacts with Drupal 8 REST Api in a simplified way.
 *
 * @version 0.1
 * @author Nino Marrazzo - reinchek
 * @copyright 2018
 * @example <caption> Init $da Api Object </caption>
 * let $da = new Api(
 *   { url: 'http://example.org' },
 *   { user: 'admin', pass: 'admin' }
 * )
 * @example <caption> Let's create an article </caption>
 * $da.create.article.setFields({
 *   title: [{ value: 'Example Article!' }],
 *   body: [{ value: 'Article's body!', format: "basic_html" }]
 * }).post()
 * @example <caption> Update it, assuming its nid is 9 </caption>
 * $da.update.article.setFields({
 *   title: [{ value: 'Updated title!' }]
 * }).post()
 * @example <caption> Delete it </caption>
 * $da.delete.article(9)
 *
 */
// https://www.qed42.com/blog/restful-drupal-primer
import axios from 'axios'

/**
 * @class Represents axios' calls adapted to Drupal 8 HAL JSON API
 * @constructor
 * @param {class} parent - The Api Class that will instantiates the Action Class
 * @return {class} this
 */
class Action {
  constructor(parent, userAuth) {
    this.parent = parent
    this.token = this.parent.getToken()
    this.headers = {
      'Content-Type': 'application/hal+json',
    }
    if (parent.userAuth !== false && userAuth) {
      this.headers = Object.assign(this.headers, {
        Authorization: parent.userAuth
      })
    }
    return this
  }

  /**
   * Adds some needed headers (like X-CSRF-Token) to the call
   * @return {class} Action instance
   */
  prepare() {
    let ref = this
    if (ref.parent.drupalUser.isLogged()) {
      ref.headers['X-CSRF-Token'] = ref.parent.drupalUser.userCsrfToken()
      return ref
    } else {
      return this.token.then(function (response) {
        ref.headers['X-CSRF-Token'] = response.data
        console.log(ref.headers['X-CSRF-Token'], 'token request')
        return ref
      })
    }
  }
}

/**
 * @class Represents an Action's class extension adapted for Drupal's entities.
 * Like Nodes, Comments, Users etc...
 * @constructor
 * @param {class} parent - Api Class that will instantiates the ActionEntity Class
 * @param {string} type - Drupal entity's type (es: 'node')
 * @param {string} bundle - Drupal entity's bundle (es: 'article')
 */
class ActionEntity extends Action {
  constructor(parent, type, bundle, userAuth = true) {
    super(parent, userAuth)
    this.type = type
    this.bundle = bundle
    this.request = {
      _links: {
        type: {
          href: `${parent.config.url}\/rest\/type\/${type}\/${bundle}`
        }
      },
      type: [{
        'target_id': bundle
      }]
    }
  }
}

/**
 * @class ActionEntity's class extension for CREATE calls
 * @constructor
 * @param {class} parent - Api Class that will instantiates the ActionEntity Class
 * @param {string} type - Drupal entity's type (es: 'node')
 * @param {string} bundle - Drupal entity's bundle (es: 'article')
 * @param {Object} fields - Drupal entity's fields (es: { title: [{ value: 'Lorem Ipsum' }]})
 * @return {class} this
 */
class ActionEntityCreate extends ActionEntity {
  constructor(parent, type, bundle, fields = null) {
    super(parent, type, bundle)
    if (fields !== null)
      this.setFields(fields)
    return this
  }
  /**
   * Set Entity's fields
   * @param {Object} fields
   * @return {class} this
   */
  setFields(fields) {
    this.fields = fields
    this.request = Object.assign(this.request, this.fields)
    return this
  }

  /**
   * Executes axios' create call
   * @return {Promise} The axios' promise
   */
  post() {
    // Prepare request with CSRF-Token header
    this.prepare()
    // Save a reference of this into ref variable
    let ref = this
    // Execute post() method and return Promise
    return axios.post(`${this.parent.config.url}/entity/${this.type}?_format=hal_json`, this.request, {
      headers: this.headers
    }).then(function (response) {
      // Entity created
      if (response.status === 201) {
        ref.node = response.data
        return ref.node
      } else { // Entity not created
        ref.node = false
        return ref.node
      }
    })
  }
}

/**
 * @class ActionEntity's class extension for DELETE calls
 * @constructor
 * @param {class} parent - Api Class that will instantiates the ActionEntity Class
 * @param {string} type - Drupal entity's type (es: 'node')
 * @param {string} bundle - Drupal entity's bundle (es: 'article')
 * @return {class} this
 */
class ActionEntityDelete extends ActionEntity {
  constructor(parent, type, bundle) {
    super(parent, type, bundle)
    return this
  }
  /**
   * Executes axios' delete call
   * @return {Promise} The axios' promise
   */
  delete(id) {
    return axios.delete(`${this.parent.config.url}/${this.type}/${id}/?_format=hal_json`, {
      headers: this.headers
    })
  }
}

/**
 * @class ActionEntity's class extension for PATCH calls
 * @constructor
 * @param {class} parent - Api Class that will instantiates the ActionEntity Class
 * @param {string} type - Drupal entity's type (es: 'node')
 * @param {string} bundle - Drupal entity's bundle (es: 'article')
 * @param {Object} fields - Drupal entity's fields (es: { title: [{ value: 'Lorem Ipsum' }]})
 * @return {class} this
 */
class ActionEntityUpdate extends ActionEntityCreate {
  constructor(parent, type, bundle, fields = null) {
    super(parent, type, bundle, fields)
  }

  /**
   * Executes axios' patch call
   * @param {number} nid - Node's nid to update
   * @return {Promise} The axios' promise
   */
  post(nid) {
    let ref = this
    return axios.patch(`${this.parent.config.url}/${this.type}/${nid}?_format=hal_json`, this.request, {
      headers: this.headers
    }).then(function (response) {
      if (response.status === 201) {
        ref.node = response.data
        return ref.node
      } else {
        ref.node = false
        return ref.node
      }
    })
  }
}

/**
 * @class ActionUser class extends generic Action Entity
 * @constructor
 * @param {class} parent - Api Class that will instantiates the ActionEntity Class
 * @param {Object} user - User's object with name and pass properties. Can be null
 * @return {class} this
 */
class ActionUser extends ActionEntity {
  constructor(parent, user = null) {
    super(parent, 'user', 'user', false)
    this.parent = parent
    this.user = user === null ? (parent.user === null ? {} : parent.user) : user
    return this
  }

  /**
   * Set user's name
   * @param name
   * @returns {ActionUser}
   */
  setName (name) {
    if (typeof this.user !== "object") this.user = {}
    this.user.name = name
    return this
  }

  /**
   * Set user's password
   * @param pass
   * @returns {ActionUser}
   */
  setPass (pass) {
    if (typeof this.user !== "object") this.user = {}
    this.user.pass = pass
    return this
  }

  /**
   * Set user's object: { name, pass }
   * @param user
   * @returns {ActionUser}
   */
  setUser (user) {
    if (typeof this.user !== "object") this.user = {}
    this.user = user
    return this
  }

  /**
   * User login action
   * @param user
   * @returns {Promise<AxiosResponse<any>>}
   */
  login(user = null) {
    if (!this.isLogged()) {
      let ref = this.parent

      if (user !== null)
        this.user = user

      ref.prepare().then(function (action) {
        return this
      })

      return axios.post(`${this.parent.config.url}/user/login?_format=hal_json`, {
        name: this.user.name,
        pass: this.user.pass
      }).then(function (data) {
        // If 200, user is logged
        if (data.status === 200) {
          let user = data.data
          ref.user.csrfToken = user.csrf_token
          ref.user.logoutToken = user.logout_token
          // Save user's token into localStorage
          localStorage.setItem('dapi.userCsrfToken', user.csrf_token)
          localStorage.setItem('dapi.userLogoutToken', user.logout_token)
          localStorage.setItem('dapi.userIsLogged', true)
          return data
        }
        return false
      })
    }
  }

  /**
   * Check if user is logged in
   * @returns {any}
   */
  isLogged() {
    return typeof localStorage.getItem('dapi.userIsLogged') !== "undefined" ? localStorage.getItem('dapi.userIsLogged') : false
  }

  /**
   * Get user's csrf token
   * @returns {any}
   */
  userCsrfToken() {
    return this.isLogged() ? localStorage.getItem('dapi.userCsrfToken') : false
  }

  /**
   * Get user's csrf logout token
   * @returns {any}
   */
  userLogoutToken() {
    return this.isLogged() ? localStorage.getItem('dapi.userLogoutToken') : false
  }

  /**
   * User logout
   */
  logout() {
    localStorage.setItem('dapi.userCsrfToken', null)
    localStorage.setItem('dapi.userLogoutToken', false)
  }
}

/**
 * @class Represents the Drupal Api class
 * @constructor
 * @param {Object} config - Api config (es: { url: 'http://localhost' })
 * @param {Object} user - Drupal user's name and password used for basic_auth (es: { name: 'admin', pass: 'admin' })
 * @return {class} this
 */
class Api {
  constructor(config, user = null) {
    this.user = user;
    this.userAuth = user !== null ? Api.getUserAuth(user.name, user.pass) : false

    this.config = config
    /**
     * Defaults entity's actions
     * Create (POST), Delete (DELETE) and Update (PATCH)
     */
    this.create = {
      page: new ActionEntityCreate(this, 'node', 'page'),
      article: new ActionEntityCreate(this, 'node', 'article')
    }
    this.delete = {
      page: new ActionEntityDelete(this, 'node', 'page'),
      article: new ActionEntityDelete(this, 'node', 'article')
    }
    this.update = {
      page: new ActionEntityUpdate(this, 'node', 'page'),
      article: new ActionEntityUpdate(this, 'node', 'article')
    }
    /**
     * User API
     */
    this.drupalUser = new ActionUser(this, this.user)

    return this
  }

  /**
   * Get Drupal CSRF-Token
   * @return {Promise} axios' promise
   */
  getToken() {
    return axios.get(`${this.config.url}/rest/session/token`)
  }

  /**
   * Build basic auth string for Authorization header
   * @param {string} name 
   * @param {string} pass 
   * @return {string}
   */
  static getUserAuth(name, pass) {
    return `Basic ${btoa(name + ':' + pass)}`
  }

  /**
   * Adds new entity's actions
   * @param {string} key - The entity's name key (es: myArticle)
   * @param {string} type - The entity's type (es: node)
   * @param {string} bundle - The entity's bundle (es: my_article)
   * @return {class} this
   */
  addEntity(key, type, bundle) {
    let newEntity = {}
    // Create
    newEntity[key] = new ActionEntityCreate(this, type, bundle)
    this.create = Object.assign(this.create, newEntity)
    // Delete
    newEntity[key] = new ActionEntityDelete(this, type, bundle)
    this.delete = Object.assign(this.delete, newEntity)
    // Update
    newEntity[key] = new ActionEntityUpdate(this, type, bundle)
    this.update = Object.assign(this.update, newEntity)

    return this
  }
}

export default Api